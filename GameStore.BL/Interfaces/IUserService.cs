﻿using AutoMapper;
using GameStore.BLL.Models;
using GameStore.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Interfaces
{
    public interface IUserService
    {
        public Task<IdentityResult> AddAsync(UserModel user);
        public Task<List<UserModel>> GetAsync();
        public Task<IdentityResult> UpdateAsync(UserModel user);
        public Task<IdentityResult> DeleteAsync(string id);
        public Task<IdentityResult> AddRoleAsync(string id, string role);
        public Task<IdentityResult> DeleteRoleAsync(string id, string role);
    }
}
