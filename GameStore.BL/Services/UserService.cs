﻿using AutoMapper;
using GameStore.BLL.Interfaces;
using GameStore.BLL.Models;
using GameStore.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BLL.Services
{
    public class UserService : IUserService
    {
        protected readonly UserManager<UserAccount> _manager;
        protected readonly IMapper _mapper;
        protected readonly RoleManager<IdentityRole<Guid>> _roleManager;

        public UserService(UserManager<UserAccount> manager, RoleManager<IdentityRole<Guid>> roleManager, IMapper mapper)
        {
            _manager = manager;
            _mapper = mapper;
            _roleManager = roleManager;
        }

        public async Task<IdentityResult> AddAsync(UserModel user)
        {
            var user_account = _mapper.Map<UserModel, UserAccount>(user);
            return await _manager.CreateAsync(user_account, user.Password);
        }

        public async Task<IdentityResult> DeleteAsync(string id)
        {
            var user = await _manager.FindByIdAsync(id);
            if (ReferenceEquals(user, null))
                return IdentityResult.Failed(new IdentityError() { Code = "404", Description = "User with id = \"" + user.Id + "\" wasn`t found" });
            return  await _manager.DeleteAsync(user);
        }

        public async Task<List<UserModel>> GetAsync()
        {
            List<UserModel> users = new List<UserModel>();
            await Task.Run(() => {
                foreach (var user in _manager.Users)
                {
                    users.Add(_mapper.Map<UserAccount, UserModel>(user));
                }
            });
            return users;
           
        }

        public async Task<IdentityResult> UpdateAsync(UserModel user)
        {
            var result_u = await _manager.FindByIdAsync(user.Id.ToString());
            if (ReferenceEquals(result_u, null))
                return IdentityResult.Failed(new IdentityError { Code = "404", Description = "User with id = \"" + user.Id + "\" wasn`t found" });
            result_u.UserName = user.FirstName;
            result_u.UserLastName = user.LastName;
            result_u.Email = user.Email;
            return await _manager.UpdateAsync(result_u);
        }

        public async Task<IdentityResult> AddRoleAsync(string id, string role)
        {
            var user = await _manager.FindByIdAsync(id);
            if (ReferenceEquals(user, null))
                return IdentityResult.Failed(new IdentityError { Code = "404", Description = "User with id = \""+ user.Id+"\" wasn`t found"});
            if(!await _roleManager.RoleExistsAsync(role))
                return IdentityResult.Failed(new IdentityError { Code = "404", Description = "Role with name = \"" + role + "\" doesn`t exist" });
            return await _manager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> DeleteRoleAsync(string id, string role)
        {
            var user = await _manager.FindByIdAsync(id);
            if (ReferenceEquals(user, null))
                return IdentityResult.Failed(new IdentityError { Code = "404", Description = "User with id = \"" + user.Id + "\" wasn`t found" });
            if (!await _roleManager.RoleExistsAsync(role))
                return IdentityResult.Failed(new IdentityError { Code = "404", Description = "Role with name = \"" + role + "\" doesn`t exist" });
            return await _manager.RemoveFromRoleAsync(user, role);
        }




    }
}
