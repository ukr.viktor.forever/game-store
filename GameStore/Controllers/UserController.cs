﻿using AutoMapper;
using GameStore.BLL.Models;
using GameStore.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using GameStore.BLL.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameStore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserController : ControllerBase
    {
        protected readonly UserService service;
        public UserController(UserManager<UserAccount> manager, RoleManager<IdentityRole<Guid>> roleManager, IMapper mapper)
        {
            service = new UserService(manager, roleManager, mapper);
        }
        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            var userList = await service.GetAsync();
            if (userList == null)
                return NotFound("There are no users");
            return Ok(userList);
        }
        [HttpPost("create")]
        public async Task<IActionResult> Create(UserModel model)
        {
            if (model == null)
                return BadRequest("Model to update user should be provided");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await service.AddAsync(model);
            if (!result.Succeeded)
                return StatusCode(500, result.Errors.ToString());
            return StatusCode(201);
        }
        [HttpPut("update")]
        public async Task<IActionResult> Update(UserModel model)
        {
            if (model == null)
                return BadRequest("Model to update user should be provided");
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await service.UpdateAsync(model);
            if(!result.Succeeded)
                return Problem(result.Errors.First().Description,null, int.Parse(result.Errors.First().Code));
            return Ok();
        }
        [HttpDelete("delete")]
        public async Task<IActionResult> Delete(string guid)
        {
            if (guid == null)
                return BadRequest("Id of user should be provided");
            var result = await service.DeleteAsync(guid);
            if (!result.Succeeded)
                return StatusCode(500, result.Errors.ToString());
            return Ok();
        }
        [HttpPut("addRole")]
        public async Task<IActionResult> AddRole(string guid, string role)
        {
            if (guid == null)
                return BadRequest(guid);
            if (role == null)
                return BadRequest(role);
            var result = await service.AddRoleAsync(guid.ToString(), role);
            if (!result.Succeeded)
                return StatusCode(500, result.Errors.ToString());
            return Ok();
        }
        [HttpDelete("deleteRole")]
        public async Task<IActionResult> DeleteRole(string guid, string role)
        {
            if (guid == null)
                return BadRequest(guid);
            if (role == null)
                return BadRequest(role);
            var result = await service.AddRoleAsync(guid.ToString(), role);
            if (!result.Succeeded)
                return StatusCode(500, result.Errors.ToString());
            return Ok();
        }

    }
}
