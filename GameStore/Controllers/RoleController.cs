﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RoleController : ControllerBase
    {
        protected readonly RoleManager<IdentityRole<Guid>> _roleManager;
        public RoleController(RoleManager<IdentityRole<Guid>> roleManager)
        {
            _roleManager = roleManager;
        }

        [HttpGet("createRole/{roleName}")]
        public async Task<IActionResult> Create(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                return BadRequest("Role name should be provided.");
            }

            var newRole = new IdentityRole<Guid>
            {
                Name = roleName
            };

            var roleResult = await _roleManager.CreateAsync(newRole);

            if (roleResult.Succeeded)
            {
                return Ok();
            }

            return StatusCode(500, roleResult.Errors.ToString());
        }

        [HttpGet("")]
        public IEnumerable<string> Get()
        {
            return _roleManager.Roles.Select(x => x.Name);
        }

        [HttpPut("update")]
        public async Task<IActionResult> Edit(string roleName, string newRoleName)
        {
            if (string.IsNullOrWhiteSpace(roleName) && string.IsNullOrWhiteSpace(newRoleName))
            {
                return BadRequest("Role name or new role name should be provided.");
            }
            var role = await _roleManager.FindByNameAsync(roleName);
            if (role is null)
            {
                return NotFound();
            }
            var result = await _roleManager.SetRoleNameAsync(role, newRoleName);
            if(result.Succeeded)
            {
                return Ok();
            }
            return StatusCode(500, result.Errors.ToString());
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                return BadRequest("Role name should be provided.");
            }
            var role = await _roleManager.FindByNameAsync(roleName);
            if (role is null)
            {
                return NotFound();
            }
            var result = await _roleManager.DeleteAsync(role);
            if (result.Succeeded)
            {
                return Ok();
            }
            return StatusCode(500, result.Errors.ToString());
        }
        
    }
}
