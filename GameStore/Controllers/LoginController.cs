﻿using GameStore.BLL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using GameStore.API.Settings;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameStore.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        protected readonly UserManager<UserAccount> _userManager;
        protected readonly IOptions<JwtSettings> _jwtSettings;
        protected readonly IMapper _mapper;

        public LoginController(UserManager<UserAccount> userManager, IOptions<JwtSettings> options, IMapper mapper)
        {
            _userManager = userManager;
            _jwtSettings = options;
            _mapper = mapper;
        }
        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(LoginModel userModel)
        {
            var user = _userManager.Users.SingleOrDefault(x => x.Email == userModel.Email);
            if (user is null)
            {
                return Unauthorized();
            }
            var SignInResult = await _userManager.CheckPasswordAsync(user, userModel.Password);
            if (SignInResult)
            {
                var roles = await _userManager.GetRolesAsync(user);
                return Ok(new { access_token = GenerateJwt(user, roles) });
            }
           return Unauthorized();
        }
        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp(UserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = _mapper.Map<UserModel, UserAccount>(model);
            var userCreateResult = await _userManager.CreateAsync(user, model.Password);
            var userAddToRoleResult = await _userManager.AddToRoleAsync(user, "User");
            if (!userCreateResult.Succeeded)
            {
                return StatusCode(500, userAddToRoleResult.Errors.ToString());
            }
            if (!userAddToRoleResult.Succeeded)
            {
                return StatusCode(500,userAddToRoleResult.Errors.ToString());
            }
            return Created(string.Empty, string.Empty);


        }
        private string GenerateJwt(UserAccount user, IList<string> roles)
        {
            var jwtSettings = _jwtSettings.Value;
            var securityKey = jwtSettings.GetSymmetricSecurityKey();
            var cregentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddSeconds(jwtSettings.TokenLifeTime);
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
            };
            var roleClaims = roles.Select(r => new Claim("role", r));
            claims.AddRange(roleClaims);
            var token = new JwtSecurityToken(jwtSettings.Issuer,
                jwtSettings.Audience,
                claims,
                expires: expires,
                signingCredentials: cregentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }


    }
}
