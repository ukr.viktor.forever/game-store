﻿using AutoMapper;
using GameStore.BLL.Models;
using GameStore.DAL.Entities;


namespace GameStore.API
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserAccount, UserModel>().ReverseMap();
        }
    }
}
