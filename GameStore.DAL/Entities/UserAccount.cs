﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameStore.DAL.Entities
{
    public class UserAccount : IdentityUser<Guid>
    {
        public string UserLastName { get; set; }
    }
}
